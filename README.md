## О программе
Приложение TaskManager разрабатывается в рамках обучения программированию на Java. 

## Требования к SOFTWARE
1. Java  11.0.7
2. Apache Maven  3

## Описание стека технологий
1. Java JDK 11.0
2. IntelliJ IDEA Community Edition 2020.1.2
3. Apache Maven  3
4. Git

## Разработчик
- Воробьев Констнантин 
- [vorobyev-ke@yandex.ru](vorobyev-ke@yandex.ru)

## Команды для сборки приложения

- mvn clean    -- удаляет директорию сборки
- mvn package  -- создание пакета 
- mvn instal   -- установка

## Команды для запуска приложения

В директории с полученным исполняемым файлом выполнить:
```java -jar ./task-manager-1.0.0.jar```

Получить список доступных команд можно выполнив команду:
```java -jar ./task-manager-1.0.0.jar help```

