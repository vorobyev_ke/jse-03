package ru.vorobyev.tm;

import static ru.vorobyev.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение для обработки аргументов запуска
 */
public class App {

    public static void main(final String[] args) {
        run(args);
        displayWelcome();
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        switch (param) {
            case ABOUT:
                displayAbout();
            case HELP:
                displayHelp();
            case VERSION:
                displayVersion();
            default:
                displayError();
        }
    }

    private static void displayError() {
        System.out.println("Error! Unknouw argument");
        System.exit(-1);
    }

    private static void displayAbout() {
        System.out.println("Konstantin Vorobyev");
        System.out.println("vorobyev-ke@yandex.ru");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        System.exit(0);
    }

    private static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.exit(0);
    }

}
